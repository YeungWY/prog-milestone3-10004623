﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3
{
    class Pizza
    {
        Dictionary<string, string> pizza = new Dictionary<string, string>();

        OrderItem[] order;
        string output;
        bool isValid;
        int PizzaTotal = 0;
        int DrinkTotal = 0;
        int total = 0;
        List<string> DrinkOrder = new List<string>();
        Client client = null;

        public void MyOrder()
        {
            //get client details
            client = new Client();
            client.UserInfo();
            Console.Clear();

            pizza.Clear();
            //build pizza menu
            pizza.Add("1", "Bacon Spinach Alfredo");
            pizza.Add("2", "Italian Meatball");
            pizza.Add("3", "Five Pepper Pepperoni");
            pizza.Add("4", "BBQ Bacon Cheeseburger");
            foreach (var item in pizza)
            {
                Console.WriteLine("{0}.  {1}", item.Key, item.Value);
            }

            Console.WriteLine("\nTo make multiple orders please enter numbers seperated by ','");
            while (true)
            {
                Console.Write("Please enter the number of the pizza that you want to order: ");
                string input = Console.ReadLine();
                var orderInput = input.Split(',');
                bool isPizza = true;

                //validate input
                Array.Resize<OrderItem>(ref order, orderInput.Length);
                int count = 0;
                foreach (var item in orderInput)
                {
                    if (!pizza.ContainsKey(item)) //if number entered is not in the menu, then exit
                    {
                        isPizza = false;
                        break;
                    }
                    else
                    {
                        order[count] = new OrderItem() { pizza = item }; //store item for later
                    }
                    count++;
                }

                if (isPizza == true)
                {
                    //all items were valid, so get the sizes
                    size();
                    break;
                }
                else
                {
                    //a pizza was invalid, so clear the array and start again 
                    Console.WriteLine("\nInValid Input! Please redo your orders!\n");
                    Array.Clear(order, 0, order.Length);
                }
            }
        }

        public void size()
        {
            Console.Clear();
            //Print the order
            Console.WriteLine("You have ordered:\n");
            foreach (var item in order)
            {
                isValid = pizza.TryGetValue(item.pizza, out output);
                Console.WriteLine(output);
            }

            //build the size menu
            Dictionary<string, int> price = new Dictionary<string, int>();
            price.Add("S", 10);
            price.Add("M", 15);
            price.Add("L", 20);

            string size = "";
            int unitPrice;


            Console.WriteLine("\n\nPlease choose the size for your pizza");
            Console.WriteLine("Small: $10;\nMedium: $15;\nLarge: $20;");

            foreach (var item in order)
            {
                isValid = pizza.TryGetValue(item.pizza, out output);
                while (true)
                {
                    Console.Write("\nSize for \"" + output + "\" (S/M/L): ");
                    size = Console.ReadLine().ToUpper();
                    if (price.ContainsKey(size)) //size is valid input, so store the size
                    {
                        bool totalPrice = price.TryGetValue(size, out unitPrice);
                        PizzaTotal += unitPrice;
                        item.size = size;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("\nInvalid Input! Please Try Again!");
                        size = string.Empty;
                    }
                }
            }

            Console.WriteLine("\nThe total price of your pizza is: $ " + PizzaTotal);

            //ask if drinks are needed
            while (true)
            {
                Console.Write("\nWould you like to order some drinks as well?(Y/N): ");
                string drink;
                drink = Console.ReadLine().ToUpper();
                if (drink == "Y")
                {
                    drinks();
                    break;
                }
                else if (drink == "N")
                {
                    confirm();
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid Input! Please Try Agian!");
                    drink = string.Empty;
                }
            }
        }

        public void drinks()
        {
            Console.Clear();

            string[] myOrder;
            string myInput;
            int i = 0;
            bool isNum = true;
            int unitPrice = 0;
            bool inValid;

            //build drinks menu
            List<Tuple<int, string, int>> drinks = new List<Tuple<int, string, int>>();
            drinks.Add(Tuple.Create(1, "Cola", 3));
            drinks.Add(Tuple.Create(2, "L&P", 2));
            drinks.Add(Tuple.Create(3, "Sprite", 3));
            drinks.Add(Tuple.Create(4, "Orange Juice", 4));

            foreach (var item in drinks)
            {
                Console.WriteLine("{0}. ${2} {1}  ", item.Item1, item.Item2, item.Item3);
            }

            Console.WriteLine("\nTo make multiple orders please enter numbers seperated by ','");

            do
            {
                inValid = false;
                Console.Write("Please enter the number of the drink that you want to order: ");
                myInput = Console.ReadLine();
                myOrder = myInput.Split(',');

                foreach (var item in myOrder)          //checks for invalid number inputs that aren't in the menu
                {
                    isNum = int.TryParse(item, out i);
                    if (i < 1 || i > 4)
                    {
                        inValid = true;
                        break;
                    }
                }

                if (inValid == true)
                {
                    Console.WriteLine("\nInvalid Input! Please Try Again!\n");
                    myInput = string.Empty;
                    break;
                }
                else
                {
                    //all the drinks are valid, so get their prices
                    foreach (var item in myOrder)
                    {
                        isNum = int.TryParse(item, out i);

                        foreach (var d in drinks)
                        {
                            if (i == d.Item1)
                            {
                                DrinkOrder.Add(d.Item2);
                                unitPrice = d.Item3;
                            }
                        }
                        DrinkTotal += unitPrice;

                    }
                }
            } while (inValid == true);

            Console.WriteLine("\n\nYou have ordered: \n");

            foreach (string s in DrinkOrder)
            {
                Console.WriteLine(s);
            }

            Console.WriteLine("\nThe total price of your drinks are: $" + DrinkTotal);
            Console.WriteLine("\nPress any key to continue...");
            Console.ReadKey();
            
            confirm();
        }


        public void confirm()
        {
            Console.Clear();

            //Reprints the pizza & drink order
            Console.WriteLine("Hello, " + client.name);
            Console.WriteLine("Phone No: " + client.phoneNo);
            Console.WriteLine("\nYou have ordered: \n");
            foreach (var item in order)
            {
                isValid = pizza.TryGetValue(item.pizza, out output);
                Console.WriteLine(output + " (Size: " + item.size + ")");
            }
            foreach (string item in DrinkOrder)
            {
                Console.WriteLine(item);
            }


            //confirm the order
            while (true)
            {
                Console.Write("\nDo you want to make changes to your order?? (Y/N): ");
                string input;
                input = Console.ReadLine().ToUpper();
                if (input == "Y")
                {
                    Console.Clear();
                    PizzaTotal = 0;
                    DrinkTotal = 0;
                    DrinkOrder.Clear();
                    MyOrder();
                    break;
                }
                else if (input == "N")
                {
                    //calculates the total cost for the user
                    total = PizzaTotal + DrinkTotal;
                    //past the total cost to the payment page
                    Payment pay = new Payment();
                    pay.Pay(total);
                    Console.WriteLine("\nWe have recieved your order! \nThank you for ordering with us!");
                    break;
                }
                else
                {
                    Console.WriteLine("Invalid Input! Please Try Again!");
                    input = string.Empty;
                }
            }
        }
    }
}
