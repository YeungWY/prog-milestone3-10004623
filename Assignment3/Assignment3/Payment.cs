﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3
{
    class Payment
    {
        public void Pay(int orderTotal)
        {
            Console.Clear();
            Console.WriteLine("******The Payment Page******");
            //print the total price for the user
            Console.WriteLine("\nThe total price for your order is: $" + orderTotal);
            while(true)
            {
                //takes the payment from the client
                Console.Write("\nPlease enter the cash value: ");
                string input = Console.ReadLine();
                double cash;
                bool isNum = double.TryParse(input, out cash);
                double change = 0.0;

                if(!isNum)
                {
                    Console.WriteLine("\nInvalid Input! Please Try Again!");
                    input = string.Empty;
                }
                else if (cash < orderTotal && cash != 0)
                {
                    Console.WriteLine("\nThe cash value is less than the total price of your order.");
                    Console.WriteLine("Please try again.");
                    input = string.Empty;
                }
                else 
                {
                    change = cash - orderTotal;
                    if(change > 0)
                    {
                        Console.WriteLine("\nPlease take your changes: $" + change);
                    }
                    break;
                }
            }      
        }
    }
}
