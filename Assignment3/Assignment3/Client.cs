﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assignment3
{
    class Client
    {
        public string name;
        public int phoneNo;

        public void UserInfo()
        {
            
            string confirm = "";

            do
            {
                Console.Clear();
                Console.Write("Please enter your name: ");
                try
                {
                    name = Console.ReadLine();
                    if (string.IsNullOrWhiteSpace(name))
                    {
                        Console.WriteLine("\nInvalid name input! Please try again!");
                        name = string.Empty;
                    }
                    else
                    {
                        while (true)
                        {
                            Console.Write("\nHi " + name + ", please enter your phone number: ");

                            string input = Console.ReadLine();
                            bool isNum = int.TryParse(input, out phoneNo);
                            if (!isNum || input.Length > 10 || input.Length < 8 || phoneNo == 0)
                            {
                                Console.WriteLine("\nInvalid number input! Please try again!");
                                input = string.Empty;
                            }
                            else
                            {
                                while (true)
                                {
                                    Console.WriteLine("\nPlease confirm your information.");
                                    Console.WriteLine("Your name is: " + name + " , your phone number is: " + phoneNo);
                                    Console.Write("\nIf your want to make changes to your information, please type in 'Y', else type in 'N': ");

                                    confirm = Console.ReadLine().ToUpper();
                                    if (confirm == "N")
                                    {
                                        break;
                                    }
                                    else if (confirm == "Y")
                                    {
                                        name = string.Empty;
                                        input = string.Empty;
                                        break;
                                    }
                                    else
                                    {
                                        Console.WriteLine("\nInvalid Input! Please try again!");
                                        confirm = string.Empty;
                                    }
                                }
                                break;
                            }
                        }
                    }
                }
                catch 
                {
                    Console.WriteLine("\nInvalid Input! Please try again!");
                    confirm = string.Empty;
                }
            } while (confirm != "N");
        }
    }
}
